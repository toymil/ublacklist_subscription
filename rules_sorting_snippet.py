#!/usr/bin/env python3

import tldextract

# li = input("paste rule list below:\n")
li = ""
l = li.splitlines()
lp = list(tldextract.extract(e[4:-2]) for e in l)
lp.sort(key=lambda x: (x.domain, x.subdomain.split(".")[::-1], x.suffix))
for e in lp:
    print(r"*://{}/*".format(".".join(x for x in e if x != "")))

# regex for removing duplicate lines
# ^(.+)(\n\1)+
# $1
